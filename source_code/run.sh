#!/bin/bash
if [ ! -d "result" ]; then
  mkdir result
fi

unbuffer roscore > result/roscore_out.txt 2> result/roscore_err.txt &
sleep 1

source devel/setup.bash
unbuffer roslaunch vtdToRos vtdToRos.launch > result/vtdToRos.txt 2> result/vtdToRos.txt &
sleep 1

source devel/setup.bash
unbuffer roslaunch rosToVtd rosToVtd.launch > result/rosToVtd.txt 2> result/rosToVtd.txt &
sleep 1

source devel/setup.bash
unbuffer roslaunch planner_s4 planning.launch > result/planning_out.txt 2> result/planning_err.txt &
sleep 1

source devel/setup.bash
unbuffer roslaunch control control.launch > result/control_out.txt 2> result/control_err.txt &
sleep 1

source devel/setup.bash
unbuffer roslaunch decision_type decision_type.launch > result/decision_type_out.txt 2> result/decision_type_err.txt
