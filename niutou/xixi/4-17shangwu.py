# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
"找出 10000以内的完美数"
for i in range(1,10001):
    a = 0
    for j in range(1,i):
        if i%j == 0:
            a +=j
        if a ==i:
            print(i)
"输出100以内所有素数"
for i in range(2,100):
    for j in range(2,i):
        if i%j==0:
            break
        else:
            print(i)
            
"""
Spyder Editor

This is a temporary script file.
"""
#"如果输入的成绩在90分以上（含90分）输出A；80分-90分（不含90分）输出B；70分-80分（不含80分）输出C；60分-70分（不含70分）输出D；60分以下输出E。"
score = float(input("请输入成绩："))
if score >= 90:
    print("成绩等级：A")
elif score >= 80:
    print("成绩等级：B")
elif score >= 70:
    print("成绩等级：C")
elif score >= 60:
    print("成绩等级：D")
else:
    print("成绩等级：E")
    
 #"英制单位英寸与公制单位厘米互换。"
value = input("请输入一个长度（数值）:")
unit = input("请输入对应的单位（英寸in or 厘米cm）：")

value = float(value)

if unit == '英寸' or unit == 'in':
    # value_convert = value * 2.54
    print("%f英寸= %f厘米" % (value,value * 2.54))
elif unit == '厘米' or unit == 'cm':
    print("%f厘米 = %f" % (value, value / 2.54))
else:
    print("请输入正确的单位！")
    
#"海伦公式计算三角形的面积"
    a = float(input("请输入第一条边长："))
b = float(input("请输入第二条边长："))
c = float(input("请输入第三条边长："))

if a + b > c and a + c > b and b +c > a:
    s = 0
    p = (a + b + c) / 2
    s = (p * (p-a) * (p-b) * (p-c)) ** 0.5
    print('三角形的面积是：%f' % (s))
else:
    print('不能构成三角形！')