"斐波拉切数字列表"
def fib(n):
    if n==1: #终止条件
        return 1
    elif n==2:
        return 1
    else: #调用条件（什么时候终止，否则就调用）
        return fib(n-1)+fib(n-2)
#输出这个列表的前6位上的数字
for i in range(1,6):

    print(fib(i))

"判断是素数还是不是素数"
def is_prime(x):
    if x==1:
        print("NO")
        return
    for i in range(2,x):
        if x%1==0:
            print("NO")
            break
    else:
        print("Yes")
n=int(input())
is_prime(n)
"计算最大公约数"
def GCD(a,b):
    # 比较大小，保证大数除以小数
    if a<b:
        a,b=b,a
    # 判断是否能整除，若能整除，直接返回被除数
    if a%b==0:
        return b
    # 若不能整除，则返回函数GCD，参数做相应变化
    else:
        return GCD(b,a%b)
a=int(input("please input the first number："))
b=int(input("please input the second number："))
gcd=GCD(a,b)
print(f"{a}和{b}的最大公约数为{gcd}")

"计算最小公倍数"

def lcm(x, y): #最小公倍数
   # 获取最大的数
   if x > y:
       greater = x
   else:
       greater = y
   while(True):
       if((greater % x == 0) and (greater % y == 0)):
           lcm = greater
           break
       greater += 1
   return lcm
# 获取用户输入
num1 = int(input("输入第一个数字: "))
num2 = int(input("输入第二个数字: "))
print( num1,"和", num2,"的最小公倍数为", lcm(num1, num2))
"""