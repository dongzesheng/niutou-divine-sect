'加减'
x=10
y=20
z=x+y
print(z)
'乘除'
a=3.14
b=2.718
c=a*b
print(c)
'字符串类型练习题'
name = "dongzesheng"
name_list = list(name)
fist_char = name_list[0]
sub_char = name_list[1:5]
upper_name = name.upper()
print(fist_char)
print(sub_char)
print(upper_name)
'列表类型练习题'
numbers = []
for i in range(1,11):
    numbers.append(i)
numbers_4_3 = numbers
numbers_4_3[4] = 100
'元组类型练习题'
colors = ("", "", "")
colors = list(colors)
colors.append("Beautiful")
colors[1] = "Girl"
colors = tuple(colors)
print(colors)
'字典类型练习题'
person = {
    "name":"dongzesheng",
    "age":""
    }
person.update({"home_town":"Enshi"})
person["age"] = 21
print(person)
del person["home_town"]
'百分制成绩转换为等级制成绩'
scoer = int(input())
if 90 <=scoer <=100:
    grade = 'A'
elif 80 <=scoer <90:
    grade = 'B'
elif 70 <=scoer <80:
    grade = 'C'
elif 60 <= scoer < 70:
    grade = 'D'
elif 0 <= scoer <60:
    grade = 'E'
else:
    grade= 'data error!'
print(grade)

