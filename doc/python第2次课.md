# Python的数据类型


Python 3 中有六个标准的数据类型：

1. Number（数字）
2. String（字符串）
3. List（列表）
4. Tuple（元组）
5. Set（集合）
6. Dictionary（字典）
Python 3 的六个标准数据类型中：

不可变数据（3 个）：Number（数字）、String（字符串）、Tuple（元组）；
可变数据（3 个）：List（列表）、Dictionary（字典）、Set（集合）。
### Number（数字）
Python 3 支持 int、float、bool、complex（复数）。

在Python 3里，只有一种整数类型 int，表示为长整型，没有 Python 2 中的 Long。

像大多数语言一样，数值类型的赋值和计算都是很直观的。

内置的 type() 函数可以用来查询变量所指的对象类型。

例如
```python
a, b, c, d = 20, 5.5, True, 4+3j
print(type(a), type(b), type(c), type(d))
```
尝试运行
```python
print(5 + 4)  # 加法
print(4.3 - 2) # 减法
print(3 * 7)  # 乘法
print(2 / 4)  # 除法，得到一个浮点数
print(2 // 4) # 除法，得到一个整数
print(17 % 3) # 取余 
print(2 ** 5) # 乘方
```
### String（字符串）
字符串是 Python 中最常用的数据类型之一，使用单引号或双引号来创建字符串，使用三引号创建多行字符串。字符串要么使用两个单引号，要么两个双引号，不能一单一双！Python 不支持单字符类型，单字符在 Python 中也是作为一个字符串使用。
字符串尝试运行
```python
print('Hello World!')
print("Jack")
print("")                   # 空字符串
print("it's apple")         # 双引号中可以嵌套单引号
print('This is "PLANE"!')   # 单引号中可以嵌套双引号
print('what is 'your'name') # 但是单引号嵌套单引号或者双引号嵌套双引号就会出现歧义
# 正确做法
# print('what is \'your\'name')
```
str.capitalize()：将字符串的第一个字符转换为大写，如果字符串中的第一个字符已经是大写，则保持不变。
str.lower()：将字符串中的所有字符转换为小写。
str.upper()：将字符串中的所有字符转换为大写。
str.count(sub[, start[, end]])：返回子字符串sub在字符串中出现的次数，可以指定搜索范围。
str.find(sub[, start[, end]])：返回子字符串sub在字符串中首次出现的索引，如果找不到子字符串，则返回-1，可以指定搜索范围。
str.replace(old, new[, count])：返回字符串的一个副本，其中出现的子字符串old被替换为new，可以选择替换次数。
str.split([sep [, maxsplit]]): 将字符串分割成多个子字符串，并返回一个子字符串列表，可以指定分隔符和最大分割次数。
str.strip([chars]): 返回去除两端字符后的字符串副本，默认去除空白字符，可以指定要去除的字符。

```python
s = 'hello world'
print(s.capitalize())  # 输出: Hello world

s = 'HELLO WORLD'
print(s.lower())  # 输出: hello world

s = 'hello world'
print(s.upper())  # 输出: HELLO WORLD

s = 'hello world'
print(s.count('l'))  # 输出: 3

s = 'hello world'
print(s.find('world'))  # 输出: 6

s = 'hello world'
print(s.replace('world', 'Python'))  # 输出: hello Python

s = 'hello world'
print(s.split(' '))  # 输出: ['hello', 'world']

s = '  hello world  '
print(s.strip())  # 输出: 'hello world'
```
#### 变量命名

对于每个变量我们需要给它取一个名字，就如同我们每个人都有属于自己的响亮的名字一样。在Python中，变量命名需要遵循以下这些必须遵守硬性规则和强烈建议遵守的非硬性规则。

- 硬性规则：
  - 变量名由字母（广义的Unicode字符，不包括特殊字符）、数字和下划线构成，数字不能开头。
  - 大小写敏感（大写的`a`和小写的`A`是两个不同的变量）。
  - 不要跟关键字（有特殊含义的单词，后面会讲到）和系统保留字（如函数、模块等的名字）冲突。
- PEP 8要求：
  - 用小写字母拼写，多个单词用下划线连接。
  - 受保护的实例属性用单个下划线开头（后面会讲到）。
  - 私有的实例属性用两个下划线开头（后面会讲到）。

Python的命名规则是在定义名称时要遵循的语法规则。在Python中，命名规则具有以下基本要求：

　　1、名称中只能包含字母、数字和下划线。

　　2、名称的第一个字符必须是字母或下划线。

　　3、名称不能以数字开头。

　　4、命名区分大小写。

当然，作为一个专业的程序员，给变量（事实上应该是所有的标识符）命名时做到见名知意也是非常重要的。

#### 变量的使用

下面通过几个例子来说明变量的类型和变量使用。

```Python
"""
使用变量保存数据并进行加减乘除运算

Version: 0.1
Author: 邢驰刚
"""
a = 321
b = 12
print(a + b)    # 333
print(a - b)    # 309
print(a * b)    # 3852
print(a / b)    # 26.75
```

在Python中可以使用`type`函数对变量的类型进行检查。程序设计中函数的概念跟数学上函数的概念是一致的，数学上的函数相信大家并不陌生，它包括了函数名、自变量和因变量。如果暂时不理解这个概念也不要紧，我们会在后续的章节中专门讲解函数的定义和使用。

```Python
"""
使用type()检查变量的类型

Version: 0.1
Author: 邢驰刚
"""
a = 100
b = 12.345
c = 1 + 5j
d = 'hello, world'
e = True
print(type(a))    # <class 'int'>
print(type(b))    # <class 'float'>
print(type(c))    # <class 'complex'>
print(type(d))    # <class 'str'>
print(type(e))    # <class 'bool'>
```

可以使用Python中内置的函数对变量类型进行转换。

- `int()`：将一个数值或字符串转换成整数，可以指定进制。
- `float()`：将一个字符串转换成浮点数。
- `str()`：将指定的对象转换成字符串形式，可以指定编码。
- `chr()`：将整数转换成该编码对应的字符串（一个字符）。
- `ord()`：将字符串（一个字符）转换成对应的编码（整数）。

下面的代码通过键盘输入两个整数来实现对两个整数的算术运算。

```Python
"""
使用input()函数获取键盘输入(字符串)
使用int()函数将输入的字符串转换成整数
使用print()函数输出带占位符的字符串

Version: 0.1
Author: 邢驰刚
"""
a = int(input('a = '))
b = int(input('b = '))
print('%d + %d = %d' % (a, b, a + b))
print('%d - %d = %d' % (a, b, a - b))
print('%d * %d = %d' % (a, b, a * b))
print('%d / %d = %f' % (a, b, a / b))
print('%d // %d = %d' % (a, b, a // b))
print('%d %% %d = %d' % (a, b, a % b))
print('%d ** %d = %d' % (a, b, a ** b))
```

> **说明**：上面的print函数中输出的字符串使用了占位符语法，其中`%d`是整数的占位符，`%f`是小数的占位符，`%%`表示百分号（因为百分号代表了占位符，所以带占位符的字符串中要表示百分号必须写成`%%`），字符串之后的`%`后面跟的变量值会替换掉占位符然后输出到终端中，运行上面的程序，看看程序执行结果就明白啦。

### 运算符

Python支持多种运算符，下表大致按照优先级从高到低的顺序列出了所有的运算符，运算符的优先级指的是多个运算符同时出现时，先做什么运算然后再做什么运算。除了我们之前已经用过的赋值运算符和算术运算符，我们稍后会陆续讲到其他运算符的使用。

| 运算符                                                       | 描述                           |
| ------------------------------------------------------------ | ------------------------------ |
| `[]` `[:]`                                                   | 下标，切片                     |
| `**`                                                         | 指数                           |
| `~` `+` `-`                                                  | 按位取反, 正负号               |
| `*` `/` `%` `//`                                             | 乘，除，模，整除               |
| `+` `-`                                                      | 加，减                         |
| `>>` `<<`                                                    | 右移，左移                     |
| `&`                                                          | 按位与                         |
| `^` `\|`                                                      | 按位异或，按位或               |
| `<=` `<` `>` `>=`                                            | 小于等于，小于，大于，大于等于 |
| `==` `!=`                                                    | 等于，不等于                   |
| `is`  `is not`                                               | 身份运算符                     |
| `in` `not in`                                                | 成员运算符                     |
| `not` `or` `and`                                             | 逻辑运算符                     |
| `=` `+=` `-=` `*=` `/=` `%=` `//=` `**=` `&=` `|=` `^=` `>>=` `<<=` | （复合）赋值运算符             |

>**说明：** 在实际开发中，如果搞不清楚运算符的优先级，可以使用括号来确保运算的执行顺序。

#### 赋值运算符

赋值运算符应该是最为常见的运算符，它的作用是将右边的值赋给左边的变量。下面的例子演示了赋值运算符和复合赋值运算符的使用。

```Python
"""
赋值运算符和复合赋值运算符

Version: 0.1
Author: 邢驰刚
"""
a = 10
b = 3
a += b        # 相当于：a = a + b
a *= a + 2    # 相当于：a = a * (a + 2)
print(a)      # 算一下这里会输出什么
```

### 比较运算符和逻辑运算符

比较运算符有的地方也称为关系运算符，包括`==`、`!=`、`<`、`>`、`<=`、`>=`，我相信没有什么好解释的，大家一看就能懂，唯一需要提醒的是比较相等用的是`==`，请注意这个地方是两个等号，因为`=`是赋值运算符，我们在上面刚刚讲到过，`==`才是比较相等的比较运算符。比较运算符会产生布尔值，要么是`True`要么是`False`。

逻辑运算符有三个，分别是`and`、`or`和`not`。`and`字面意思是“而且”，所以`and`运算符会连接两个布尔值，如果两个布尔值都是`True`，那么运算的结果就是`True`；左右两边的布尔值有一个是`False`，最终的运算结果就是`False`。相信大家已经想到了，如果`and`左边的布尔值是`False`，不管右边的布尔值是什么，最终的结果都是`False`，所以在做运算的时候右边的值会被跳过（短路处理），这也就意味着在`and`运算符左边为`False`的情况下，右边的表达式根本不会执行。`or`字面意思是“或者”，所以`or`运算符也会连接两个布尔值，如果两个布尔值有任意一个是`True`，那么最终的结果就是`True`。当然，`or`运算符也是有短路功能的，在它左边的布尔值为`True`的情况下，右边的表达式根本不会执行。`not`运算符的后面会跟上一个布尔值，它的作用是得到与该布尔值相反的值，也就是说，后面的布尔值如果是`True`运算结果就是`False`，而后面的布尔值如果是`False`则运算结果就是`True`。

```Python
"""
比较运算符和逻辑运算符的使用

Version: 0.1
Author: 邢驰刚
"""
flag0 = 1 == 1
flag1 = 3 > 2
flag2 = 2 < 1
flag3 = flag1 and flag2
flag4 = flag1 or flag2
flag5 = not (1 != 2)
print('flag0 =', flag0)    # flag0 = True
print('flag1 =', flag1)    # flag1 = True
print('flag2 =', flag2)    # flag2 = False
print('flag3 =', flag3)    # flag3 = False
print('flag4 =', flag4)    # flag4 = True
print('flag5 =', flag5)    # flag5 = False
```

> **说明**：比较运算符的优先级高于赋值运算符，所以`flag0 = 1 == 1`先做`1 == 1`产生布尔值`True`，再将这个值赋值给变量`flag0`。`print`函数可以输出多个值，多个值之间可以用`,`进行分隔，输出的内容之间默认以空格分开。

### 练习

#### 练习1：华氏温度转换为摄氏温度。

> 提示：华氏温度到摄氏温度的转换公式为：$C=(F - 32) \div 1.8$。

参考答案：

```Python
"""
将华氏温度转换为摄氏温度

Version: 0.1
Author: 邢驰刚
"""
f = float(input('请输入华氏温度: '))
c = (f - 32) / 1.8
print('%.1f华氏度 = %.1f摄氏度' % (f, c))
```

> **说明**：在使用`print`函数输出时，也可以对字符串内容进行格式化处理，上面`print`函数中的字符串`%.1f`是一个占位符，稍后会由一个`float`类型的变量值替换掉它。同理，如果字符串中有`%d`，后面可以用一个`int`类型的变量值替换掉它，而`%s`会被字符串的值替换掉。除了这种格式化字符串的方式外，还可以用下面的方式来格式化字符串，其中`{f:.1f}`和`{c:.1f}`可以先看成是`{f}`和`{c}`，表示输出时会用变量`f`和变量`c`的值替换掉这两个占位符，后面的`:.1f`表示这是一个浮点数，小数点后保留1位有效数字。
>
> ```Python
> print(f'{f:.1f}华氏度 = {c:.1f}摄氏度')
> ```

#### 练习2：输入圆的半径计算计算周长和面积。

参考答案：

```Python
"""
输入半径计算圆的周长和面积

Version: 0.1
Author: 邢驰刚
"""
radius = float(input('请输入圆的半径: '))
perimeter = 2 * 3.1416 * radius
area = 3.1416 * radius * radius
print('周长: %.2f' % perimeter)
print('面积: %.2f' % area)
```

#### 练习3：输入年份判断是不是闰年。

参考答案：

```Python
"""
输入年份 如果是闰年输出True 否则输出False

Version: 0.1
Author: 邢驰刚
"""
year = int(input('请输入年份: '))
# 如果代码太长写成一行不便于阅读 可以使用\对代码进行折行
is_leap = year % 4 == 0 and year % 100 != 0 or \
          year % 400 == 0
print(is_leap)
```

> **说明**：比较运算符会产生布尔值，而逻辑运算符`and`和`or`会对这些布尔值进行组合，最终也是得到一个布尔值，闰年输出`True`，平年输出`False`。
### List（列表）
List（列表） 是 Python 中使用最频繁的数据类型。

列表可以完成大多数集合类的数据结构实现。列表中元素的类型可以不相同，它支持数字，字符串甚至可以包含列表（所谓嵌套）。

列表是写在方括号 [] 之间、用逗号分隔开的元素列表。
尝试运行
```python
print([])
print(list())
```
#### python 列表的内置函数
Python 列表包含许多内建函数，以下是一些常用的：

len(list)：返回列表的长度。

max(list)：返回列表的最大值。

min(list)：返回列表的最小值。

list(seq)：将元组转换为列表。

list.append(obj)：在列表末尾添加一个新的对象。

list.count(obj)：统计某个元素在列表中出现的次数。

list.extend(seq)：通过添加另一个序列中的元素来扩展列表。

list.index(obj)：从列表中找出某个值第一个匹配项的索引。

list.insert(index, obj)：将对象插入列表。

list.pop([index])：移除列表中的一个元素（默认最后一个），并返回该元素。

list.remove(obj)：移除列表中某个值的第一个匹配项。

list.reverse()：反向列表中元素。

list.sort(key=None, reverse=False)：对原列表进行排序。

list.clear()：清空列表。

list.copy()：浅复制列表。

示例代码：
```python
# 创建一个列表
my_list = [1, 2, 3, 4, 5]
 
# 使用内建函数
print(len(my_list))  # 输出列表长度
print(max(my_list))  # 输出列表最大值
print(min(my_list))  # 输出列表最小值
 
# 列表转换
my_tuple = (1, 2, 3)
my_list_from_tuple = list(my_tuple)
print(my_list_from_tuple)  # 输出: [1, 2, 3]
 
# 添加元素
my_list.append(6)
print(my_list)  # 输出: [1, 2, 3, 4, 5, 6]
 
# 统计元素
print(my_list.count(1))  # 输出: 1
 
# 扩展列表
my_list.extend([7, 8])
print(my_list)  # 输出: [1, 2, 3, 4, 5, 6, 7, 8]
 
# 查找元素索引
print(my_list.index(3))  # 输出: 2
 
# 插入元素
my_list.insert(2, 'a')
print(my_list)  # 输出: [1, 2, 'a', 3, 4, 5, 6, 7, 8]
 
# 移除元素
my_list.pop()
print(my_list)  # 输出: [1, 2, 'a', 3, 4, 5, 6, 7]
 
# 移除特定元素
my_list.remove(5)
print(my_list)  # 输出: [1, 2, 'a', 3, 4, 6, 7]
 
# 反转列表
my_list.reverse()
print(my_list)  # 输出: [7, 6, 4, 3, 'a', 2, 1]
 
# 排序列表
my_list.sort()
print(my_list)  # 输出: [1, 2, 3, 4, 6, 7]
 
# 清空列表
my_list.clear()
print(my_list)  # 输出: []
 
# 浅复制列表
copied_list = my_list.copy()
print(copied_list)  # 输出
```

### Tuple（元组）
元组（tuple）与列表类似，不同之处在于元组的元素不能修改。元组写在小括号 () 里，元素之间用逗号隔开。

元组中的元素类型也可以不相同：
```python
tuple = ( 'abcd', 786 , 2.23, 'lintcode', 70.2  )
tinytuple = (123, 'lintcode')

print(tuple)          # 输出完整元组
print(tuple[0])       # 输出元组的第一个元素
print(tuple[1:3])     # 输出从第二个元素开始到第三个元素
print(tuple[2:])      # 输出从第三个元素开始的所有元素
print(tinytuple * 2)  # 输出两次元组
print(tuple + tinytuple) # 连接元组
```


### Set（集合）
集合（set）是由一个或数个形态各异的大小整体组成的，构成集合的事物或对象称作元素或是成员。

基本功能是进行成员关系测试和删除重复元素。

可以使用大括号 {} 或者 set() 函数创建集合，注意：创建一个空集合必须用 set() 而不是 {}，因为 {} 是用来创建一个空字典。

创建格式：
```
parame = {value01,value02,...}
或者
set(value)
```
```python
a = set({'Zhihu', 'Baidu', 'Taobao', 'Jiuzhang', 'Google', 'Facebook'})
print(a)

if 'Jiuzhang' in a:
    print('Jiuzhang 在集合中')
else:
    print('Jiuzhang 不在集合中')

a = set('bcard')
b = {'r', 'b', 'd'}

print(a)
print(b)
print(a - b)
print(a | b)
print(a & b)
print(a ^ b)
```
Python中的集合（set）是一种无序且不重复的数据结构，它可以用于存储多个元素。集合可以用于解决很多问题
```python
# 去重：集合可以快速去重，只保留不重复的元素。
nums = [1, 2, 3, 3, 4, 4, 5]
unique_nums = set(nums)
print(unique_nums)  # {1, 2, 3, 4, 5}

# 判断成员关系：集合可以用来判断一个元素是否存在于集合中，其时间复杂度为O(1)。
fruits = {'apple', 'banana', 'orange'}
print('apple' in fruits)  # True
print('grape' in fruits)  # False

# 集合运算：集合支持一些基本的集合运算，如并集、交集和差集。
set1 = {1, 2, 3}
set2 = {2, 3, 4}
union_set = set1 | set2
intersection_set = set1 & set2
difference_set = set1 - set2
print(union_set)  # {1, 2, 3, 4}
print(intersection_set)  # {2, 3}
print(difference_set)  # {1}

# 数据筛选：可以使用集合进行数据筛选，例如过滤掉某些特定的元素。
nums = [1, 2, 3, 4, 5]
exclude_set = {2, 3}
filtered_nums = [num for num in nums if num not in exclude_set]
print(filtered_nums)  # [1, 4, 5]

# 字符统计：可以使用集合来统计一个字符串中不重复字符的个数。
s = 'hello world'
unique_chars = set(s)
print(len(unique_chars))  # 8
```

### Dictionary（字典）
字典（dictionary）是Python中另一个非常有用的内置数据类型。
列表是有序的对象集合，字典是无序的对象集合。两者之间的区别在于：字典当中的元素是通过键来存取的，而不是通过偏移存取。
字典是一种映射类型，字典用 { } 标识，它是一个无序的 键(key) : 值(value) 的集合。
键(key)必须使用不可变类型。在同一个字典中，键(key)必须是唯一的。
```python
dict = {}
dict['one'] = "领扣题目"
dict[2] = "领扣学习"

tinydict = {'name': 'lintcode', 'code': 1, 'site': 'www.lintcode.com'}


print(dict['one'])    # 输出键为 'one' 的值
print(dict[2])        # 输出键为 2 的值
print(tinydict)       # 输出完整的字典
print(tinydict.keys())   # 输出所有键
print(tinydict.values())  # 输出所有值
```
另外，字典类型也有一些内置的函数，例如 clear()、keys()、values() 等。

注意：

1、字典是一种映射类型，它的元素是键值对。
2、字典的关键字必须为不可变类型，且不能重复。
3、创建空字典使用 {}。

```python
# 创建一个字典
person = {
    'name': 'Alice',
    'age': 25,
    'city': 'New York'
}

字典（dict）在Python中是内置的数据类型，它是一个可变的容器模型，以键值对（key-value）的形式存储数据。字典的用途非常广泛，以下是一些主要的用途：

1. 存储键值关联的数据：字典可以存储不同的数据类型，每个元素都有唯一的键与之对应，通过键可以快速访问与该键关联的值。
2. 配置/设置存储：可以使用字典来存储程序的配置或设置，通过键来访问相应的值。
3. 模拟数据库：Python中的字典可以模拟简单的数据库，存储和检索数据。
4. 数据结构：字典可以用作其他复杂数据结构（如树）的组件。
5. 动态命名：字典的键可以是任何不可变的数据类型，这使得它们可以用于动态命名，如格式字符串或反射。

以下是一个简单的字典用法示例：
# 访问字典的值
print(person['name'])  # 输出: Alice
print(person['age'])   # 输出: 25
 
# 修改字典的值
person['age'] = 26
 
# 添加新的键值对
person['email'] = 'alice@example.com'
 
# 删除键值对
del person['city']
 
# 遍历字典
for key, value in person.items():
    print(key, value)
```